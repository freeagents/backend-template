import { schema } from 'nexus';

schema.queryType({
  definition(t) {
    t.crud.user();
    t.crud.users();
    t.crud.business();
    t.crud.businesses();
    t.crud.service();
    t.crud.services();
    t.crud.appointments();
  },
});
