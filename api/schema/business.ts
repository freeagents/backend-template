import { schema } from 'nexus';

schema.objectType({
  name: 'Business',
  definition(t) {
    t.model.id();
    t.model.name();
    t.model.email();
    t.model.password();
    t.model.street();
    t.model.city();
    t.model.zip();
    t.model.open();
    t.model.close();
    t.model.Service();
  },
});
