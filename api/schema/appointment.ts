import { schema } from 'nexus';

schema.objectType({
  name: 'Appointment',
  definition(t) {
    t.model.id();
    t.model.created();
    t.model.date_start();
    t.model.apt_user();
    t.model.apt_service();
    t.model.User();
    t.model.Service();
  },
});
