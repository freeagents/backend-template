import { schema } from 'nexus';

schema.objectType({
  name: 'User',
  definition(t) {
    t.model.id();
    t.model.first_name();
    t.model.last_name();
    t.model.email();
    t.model.password();
    t.model.phone();
    t.model.Appointment();
  },
});
