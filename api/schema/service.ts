import { schema } from 'nexus';

schema.objectType({
  name: 'Service',
  definition(t) {
    t.model.id();
    t.model.name();
    t.model.duration();
    t.model.price();
    t.model.provider();
    t.model.Business();
  },
});
