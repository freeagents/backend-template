import { schema } from 'nexus';

schema.mutationType({
  definition(t) {
    t.crud.createOneUser();
    t.crud.createOneBusiness();
    t.crud.createOneService();
    t.crud.createOneAppointment();
  },
});
