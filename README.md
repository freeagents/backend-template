# Backend Template

## Tech Stack
- [Typescript](https://www.typescriptlang.org/)
- [GraphQL](https://graphql.org/)
- [Nexus](https://nexusjs.org/)
- [Prisma](https://www.prisma.io/)

## Instructions
1. Download Repo and open in your IDE.
2. Rename example.env to .env
2. Ask Juan for database URL and replace string with given URL.
3. Finally run the following commands:
```
yarn install
yarn start
```
GraphQL playground will be on:

[http://localhost:4000/graphql](http://localhost:4000/graphql)

Optional:
To view entries in database, run:
```
yarn prisma studio --experimental
```
View entries on:

[http://localhost:5555](http://localhost:5555)

## Queries
Make queries to API using this schema:
_Note: Not all properties are needed in Queries_

- Retrieves all Businesses from database.
```graphql
{
  businesses {
    id
    name
    email
    street
    city
    zip
  }
}
```
- Will retrieve only the user that matches emails with the passed argument and return all of their appointments.
```graphql
{
  user(where: { email: "jimmyG10@gmail.com" }) {
    Appointment {
      id
      apt_date_start
      Service {
        service_name
        duration
      }
    }
  }
}
```

## Mutations
Some Mutations the that can be performed. Mutations will allow you to Post, Update, & Delete to the database.

- Create a new user, returns uid.
```graphql
mutation {
  createOneUser(
    data: {
      first_name: "Jimmy"
      last_name: "Garoppolo"
      email: "jimmyG10@gmail.com"
      phone: "8886543210"
      password: "somePassword"
    }
  ) {
    id
  }
}
```
- Create a new business, returns uid.
```graphql
mutation {
  createOneBusiness(
    data: {
      name: "Barbershop"
      email: "joe@barbershop.com"
      password: "somePassword"
      street: "10 Fade Lane"
      city: "Seattle"
      zip: 98101
      open: "00:09:00"
      close: "00:17:00"
    }
  ) {
    id
  }
}
```
- Create a new service connected to a business, returns uid.
```graphql
mutation {
  createOneService(
    data: {
      service_name: "Men's Cut"
      duration: 60
      price: 20
      Business: { connect: { id: "UID OF BUSINESS" } }
    }
  ) {
    id
  }
}
```
- Create a new service connected to a business, returns uid. `apt_date_start` will be in ISO 3339 format with 3 digits of second fraction. Example: [ISO RFC 3339](https://date-fns.org/v2.16.1/docs/formatRFC3339).
```graphql
mutation {
  createOneAppointment(
    data: {
      apt_date_start: "2020-09-25T13:00:00.000Z"
      User: { connect: { id: "UID OF USER" } }
      Service: { connect: { id: "UID OF SERVICE" } }
    }
  ) {
    id
  }
}

```